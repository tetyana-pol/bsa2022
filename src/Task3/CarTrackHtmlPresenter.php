<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\Car;
use App\Task1\Track;

class CarTrackHtmlPresenter
{
    public function present(Track $track): string
    {
       
        $info = '<div>';
        $cars = $track->all();
        foreach ($cars as $car){
            $info .= '<ul>';
            $info .= '<li>Name:' . $car->getName().'<img src="'.$car->getImage().'"></li>'; 
            $info .= '<li">Speed: ' . $car->getSpeed() . ' km/h' .'</li>';
            $info .= '<li>Pit Stop Time: ' . $car->getPitStopTime() . ' seconds' .'</li>';
            $info .= '<li>Fuel Consumption: ' . $car->getFuelConsumption() . ' litres'. '</li>';
            $info .= '<li>Fuel Tank Volume: ' . $car->getFuelTankVolume() . ' litres'. '</li>';
            $info .= '</ul>';
        }
        $info .= '</div>';
        return $info;
    }
}