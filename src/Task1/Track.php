<?php

declare(strict_types=1);

namespace App\Task1;
use App\Task1\Car;

class Track
{
    private $lapLength;
    private $lapsNumber;
    private $list = [];
    public function __construct(float $lapLength, int $lapsNumber)
    {
        
        $this->lapLength = $lapLength;
        $this->lapsNumber = $lapsNumber;
        
    }

    public function getLapLength(): float
    {
        
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        
        return $this->lapsNumber;
    }

    public function add(Car $car): void
    {
        
        $this->list[]=$car;
    }

    public function all(): array
    {
        
        return $this->list;

    }

    public function run(): Car
    {
        
        
      
       $newList =array_map('count', $this->all());
       return $this->all()[array_search(min($newList), $newList)];
       
    }
    public function count(Car $car): number
    {
        $time = $this->getLapLength()*($this->getLapsNumber())/$car->speed;
        $countLitr = $this->getLapLength()*($this->getLapsNumber())/100*
           ($car->getFuelConsumption());
        $time = $time - $countLitr*$car->getPitStopTime()/(3600*car->getFuelTankVolume());
    return $time;
    }
}